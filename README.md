# ZDoccer

ZDoccer is a script that collects all the Javadoc documentation from your
source code and puts it into a `README.md` file

## Installation

To install zdoccer, run
```
npm i -D @zimatech/zdoccer
```

## Usage

To doc a project folder, run
```
npx zdoccer <DIRECTORY>
```

To exclude a directory from being docced, use the `+i=<GLOB>` flag, e.g:
```
npx zdoccer src +i=PHPMailer
```

Default ignore rules include `.git`, `node_modules`, `polyfills.ts`, `e2e` and
the output filename.

To change the output filename, use the `-o=<FILENAME>` flag, e.g:
```
npx zdoccer src -o=TEST.md
```
