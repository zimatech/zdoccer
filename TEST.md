This file was auto-generated with `zdoccer.js` 2.0.3

# Index

  - [ZDoccer](#zdoccer)
    - [Build](#build)
    - [Publish](#publish)
    - [Update `TEST.md`](#update-test-md)
    - [`class ZDoccer`](#class-zdoccer)
      - [`constructor()`](#constructor)
      - [`private resolveLinks()`](#private-resolvelinks)
      - [`private write()`](#private-write)
    - [`class ZDocLocation`](#class-zdoclocation)
      - [`constructor(path: string)`](#constructor-path-string)
      - [`public isReady(): Promise<void>`](#public-isready-promise-void)
      - [`private scan(): Promise<void>`](#private-scan-promise-void)
      - [`public doc()`](#public-doc)
    - [`class ZDocFile`](#class-zdocfile)
      - [`public static files: ZDocFile[] = []`](#public-static-files-zdocfile)
      - [`public path: string`](#public-path-string)
      - [`public markdown: string`](#public-markdown-string)
      - [`public headings: ZDocHeading[] = []`](#public-headings-zdocheading)
      - [`constructor(path: string, markdown: string, header: string)`](#constructor-path-string-markdown-string-header-string)
      - [`public resolveLinks()`](#public-resolvelinks)
      - [`public getContent(): string`](#public-getcontent-string)
    - [`class ZDocHeading`](#class-zdocheading)
      - [`public static ids: string[] = []`](#public-static-ids-string)
      - [`public text: string`](#public-text-string)
      - [`public level: number`](#public-level-number)
      - [`public id: string`](#public-id-string)
      - [`public alias: string`](#public-alias-string)
      - [`constructor(heading: string, level: number, alias: string)`](#constructor-heading-string-level-number-alias-string)
      - [`public static createId(text: string): string`](#public-static-createid-text-string-string)
    - [`class ZDocJavadocSource`](#class-zdocjavadocsource)
      - [`constructor(path: string)`](#constructor-path-string--2)
      - [`private docMatch(match: RegExpMatchArray): string`](#private-docmatch-match-regexpmatcharray-string)
    - [`class ZDocMarkdownSource`](#class-zdocmarkdownsource)
      - [`constructor(path: string)`](#constructor-path-string--3)
  - [Doccing .css and .scss files](#doccing-css-and-scss-files)
    - [`html`](#html)
    - [`.test, .tester, .testest`](#test-tester-testest)
    - [Themes](#themes)
      - [`--default: #000000`](#default-000000)
    - [`[conflict]`](#conflict)
  - [Doccing .php files](#doccing-php-files)
    - [`echo('hello world')`](#echo-hello-world)
    - [`class Conflict`](#class-conflict)
      - [`public async thisOnesAsync()`](#public-async-thisonesasync)


---

*original Markdown from src/zdoccer.md*

<div id="zdoccer"></div><!-- alias: zdoccer -->

# ZDoccer

This plain typescript project was initialized with
```
tsc --init
npm init
npm i @types/node --save-dev
```

<div id="build"></div><!-- alias: build -->

## Build

To build, run
```
npm run build
```

<div id="publish"></div><!-- alias: publish -->

## Publish

To publish ZDoccer, run
```
npm publish --access=public
```

<div id="update-test-md"></div><!-- alias: update-test-md -->

## Update `TEST.md`

To do a test doc run, run
```
npm run test
```


---

*transformed Javadoc from src/zdoccer.ts*

<div id="class-zdoccer"></div><!-- alias: zdoccer -->

## `class ZDoccer`


ZDoccer application.

Does:
- interpret command line
- scan directories for input files (source candidates and documentation) and sort them
- transform javadoc source files through [ZDocJavadocSource **&#x1f875;**](#class-zdocjavadocsource)
- transform markdown files through [ZDocMarkdownSource **&#x1f875;**](#class-zdocmarkdownsource)
- generate file headers and heading anchors
- resolve \[\[doc-style-links\]\]
- concat generated docs
- create index
- write README file


<div id="constructor"></div><!-- alias: constructor -->

### `constructor()`

Start new ZDoccer application

<div id="private-resolvelinks"></div><!-- alias: resolvelinks -->

### `private resolveLinks()`


Resolve \[\[doc-style-links\]\] in markdown


<div id="private-write"></div><!-- alias: write -->

### `private write()`


Write README file


<div id="class-zdoclocation"></div><!-- alias: zdoclocation -->

## `class ZDocLocation`


Locations to check for docs and markdown


<div id="constructor-path-string"></div><!-- alias: constructor -->

### `constructor(path: string)`


Create new `ZDocLocation`.
Determine whether location is file or folder.
Folders are read recursively.
- *param* `path` &mdash; path to start from


<div id="public-isready-promise-void"></div><!-- alias: isready -->

### `public isReady(): Promise<void>`


Whether the location is ready (to be read).
- *returns* &mdash; promise resolving when ready


<div id="private-scan-promise-void"></div><!-- alias: scan -->

### `private scan(): Promise<void>`


Scan directory recursively for locations. Sort them.
- *returns* &mdash; promise resolving once all children are ready


<div id="public-doc"></div><!-- alias: doc -->

### `public doc()`

Doc this location and children. Invokes appropriate doccer

<div id="class-zdocfile"></div><!-- alias: zdocfile -->

## `class ZDocFile`


Files to include in output, markdowned.
Every newly created `ZDocFile` is automatically stored in internal storage.


<div id="public-static-files-zdocfile"></div><!-- alias: files -->

### `public static files: ZDocFile[] = []`

all files

<div id="public-path-string"></div><!-- alias: path -->

### `public path: string`

this file's location

<div id="public-markdown-string"></div><!-- alias: markdown -->

### `public markdown: string`

this file's markdown content

<div id="public-headings-zdocheading"></div><!-- alias: headings -->

### `public headings: ZDocHeading[] = []`

this file's linkable sections

<div id="constructor-path-string-markdown-string-header-string"></div><!-- alias: constructor -->

### `constructor(path: string, markdown: string, header: string)`


Create new file with content.
- *param* `path` &mdash; path to the file (to display in header)
- *param* `markdown` &mdash; markdown content of this file
- *param* `header` &mdash; additional header info, content description


<div id="public-resolvelinks"></div><!-- alias: resolvelinks -->

### `public resolveLinks()`

Resolve \[\[doc-style-links\]\](optional-link-target) in markdown

<div id="public-getcontent-string"></div><!-- alias: getcontent -->

### `public getContent(): string`

Emit this file's contents

<div id="class-zdocheading"></div><!-- alias: zdocheading -->

## `class ZDocHeading`


Headings in files.


<div id="public-static-ids-string"></div><!-- alias: ids -->

### `public static ids: string[] = []`

storage of all existing heading identifiers to prevent collisions

<div id="public-text-string"></div><!-- alias: text -->

### `public text: string`

text to display

<div id="public-level-number"></div><!-- alias: level -->

### `public level: number`

level

<div id="public-id-string"></div><!-- alias: id -->

### `public id: string`

id (unique)

<div id="public-alias-string"></div><!-- alias: alias -->

### `public alias: string`

alias to be found by

<div id="constructor-heading-string-level-number-alias-string"></div><!-- alias: constructor -->

### `constructor(heading: string, level: number, alias: string)`

Create and register new `ZDocHeading` from markdown heading

<div id="public-static-createid-text-string-string"></div><!-- alias: createid -->

### `public static createId(text: string): string`

Create `id`-value from text

<div id="class-zdocjavadocsource"></div><!-- alias: zdocjavadocsource -->

## `class ZDocJavadocSource`


Javadoc source transformer


<div id="constructor-path-string--2"></div><!-- alias: constructor -->

### `constructor(path: string)`


Create new transformer.
Parse file for Javadoc documented declarations and transform them to
markdown.
If result is not empty, it's stored as [ZDocFile **&#x1f875;**](#class-zdocfile)


<div id="private-docmatch-match-regexpmatcharray-string"></div><!-- alias: docmatch -->

### `private docMatch(match: RegExpMatchArray): string`


Turn a Javadoc + declaration match into markdown


<div id="class-zdocmarkdownsource"></div><!-- alias: zdocmarkdownsource -->

## `class ZDocMarkdownSource`


Markdown source transformer


<div id="constructor-path-string--3"></div><!-- alias: constructor -->

### `constructor(path: string)`


Create new transformer.
Parse file for markdown.
If result is not empty, it's stored as [ZDocFile **&#x1f875;**](#class-zdocfile)




---

*transformed Javadoc from src/test/testli.css*


<div id="doccing-css-and-scss-files"></div><!-- alias: doccing-css-and-scss-files -->

# Doccing .css and .scss files
Can be done using Javadoc. Most IDEs don't provide Javadoc support for css,
just write the starting <code>&#47;**</code> and the ending <code>*&#47;</code> on your own.


<div id="html"></div><!-- alias: html -->

## `html`


generic css tag rule


<div id="test-tester-testest"></div><!-- alias: test-tester-testest -->

## `.test, .tester, .testest`


class rules



<div id="themes"></div><!-- alias: themes -->

## Themes


<div id="default-000000"></div><!-- alias: default -->

### `--default: #000000`

Default color scheme

<div id="conflict"></div><!-- alias: conflict -->

## `[conflict]`


conflicting css declaration [conflict **&#x1f875;**](#conflict)




---

*original Markdown from src/test/testli.md*

Should be ambiguous: [Conflict **&#x26a0;**](#conflict)

Should not be ambiguous: [Conflict **&#x1f875;**](#class-conflict)

Should not be ambiguous: [Conflict **&#x1f875;**](#conflict)

Should not be resolved: [class-no-target-here **&#x29b8;**](#)

Should not be resolved: [Conflict **&#x29b8;**](#)

Should be marked as weak: [php **?**](#doccing-php-files)

---

*transformed Javadoc from src/test/testli.php*


<div id="doccing-php-files"></div><!-- alias: doccing-php-files -->

# Doccing .php files

Can be done using Javadoc


<div id="echo-hello-world"></div><!-- alias: echo -->

## `echo('hello world')`


See? This was docced perfectly fine.




---

*transformed Javadoc from src/test/testli.ts*

<div id="class-conflict"></div><!-- alias: conflict -->

## `class Conflict`


conflicting class [conflict **&#x1f875;**](#class-conflict)


<div id="public-async-thisonesasync"></div><!-- alias: thisonesasync -->

### `public async thisOnesAsync()`

this function is marked as async but should be resolvable [thisOnesAsync **&#x1f875;**](#public-async-thisonesasync)

