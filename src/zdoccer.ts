import * as fs from 'fs';
import { default as minimatch } from 'minimatch';

/**
 * ZDoccer application.
 * 
 * Does:
 * - interpret command line
 * - scan directories for input files (source candidates and documentation) and sort them
 * - transform javadoc source files through [[ZDocJavadocSource]]
 * - transform markdown files through [[ZDocMarkdownSource]]
 * - generate file headers and heading anchors
 * - resolve \[\[doc-style-links\]\]
 * - concat generated docs
 * - create index
 * - write README file
 */
class ZDoccer {
  public static version: string = "2.0.3";
  public static javadocExtensions: string[] = ['ts','js','php','css','scss'];
  public static markdownExtensions: string[] = ['md'];
  public static ignores: string[] = ['.git', 'node_modules', 'polyfills.ts', 'e2e'];
  public static mangles: string[] = ['^export ', ' implements .*'];
  public static manglesRegex: RegExp[] = [];
  public static outFilename: string = "README.md";

  /** Start new ZDoccer application */
  constructor() {
    // interpret command line
    let args = process.argv.slice(2);
    let rootPath = args[0];
    if (!fs.statSync(rootPath).isDirectory()) {
      console.log('invalid root path');
      return;
    }
    args.splice(0, 1);
    for (let arg of args) {
      // split argument into command and param, then interpret
      // replacer returns empty string if successfull
      arg = arg.replace(/([-+]\w+)=(.*)/, (match, cmd, param) => {
        switch (cmd) {
          // add ignore rule
          case '+i':
            ZDoccer.ignores.push(param);
            return '';
          // set output filename
          case '-o':
            ZDoccer.outFilename = param;
            return '';
        }
        return match;
      });
      if (arg) {
        console.log('could not understand argument: ' + arg);
      }
    }
    console.log('running ZDoccer ' + ZDoccer.version);
    // append output file to ignore set
    const ignoreOut = ZDoccer.outFilename.replace(/^\.\//, ''); // replace leading ./
    ZDoccer.ignores.push(ignoreOut);
    // turn mangle rules into regex
    for (const mangle of ZDoccer.mangles) {
      ZDoccer.manglesRegex.push(new RegExp(mangle));
    }
    // scan directories (recursively) for input files. sorts.
    let root = new ZDocLocation(rootPath);
    root.isReady().then(() => {
      // extract documentations, transform to markdown, generate file headers
      root.doc();
      // resolve links
      this.resolveLinks();
      // create and write output file
      this.write();
    });
  }

  /**
   * Resolve \[\[doc-style-links\]\] in markdown
   */
  private resolveLinks() {
    for (const file of ZDocFile.files) {
      file.resolveLinks();
    }
  }

  /**
   * Write README file
   */
  private write() {
    let contents: string = 'This file was auto-generated with `zdoccer.js` ' + ZDoccer.version + '\n\n';
    // start with index
    contents += '# Index\n\n';
    for (const file of ZDocFile.files) {
      for (const h of file.headings) {
        contents += '  '.repeat(h.level) + '- ' +
          '[' + h.text + '](#' + h.id + ')\n';
      }
    }
    // append all markdown content
    console.log('files docced:', ZDocFile.files.length);
    for (const file of ZDocFile.files) {
      contents += file.getContent();
    }
    // write file
    console.log('writing to', ZDoccer.outFilename);
    fs.writeFileSync(ZDoccer.outFilename, contents);
  }
}

/**
 * Locations to check for docs and markdown
 */
class ZDocLocation {
  public path: string;
  public stats: fs.Stats;
  public children: ZDocLocation[] | undefined;
  private _ready: Promise<void>;

  /**
   * Create new `ZDocLocation`.
   * Determine whether location is file or folder.
   * Folders are read recursively.
   * @param path path to start from
   */
  constructor(path: string) {
    this.path = path;
    this.stats = new fs.Stats();
    this._ready = new Promise((resolve, reject) => {
      fs.stat(this.path, (err, stats) => {
        this.stats = stats;
        if (this.stats.isFile()) {
          // files resolve immediately
          resolve();
        }
        else {
          // direcotires resolve when scanned
          this.scan().then(() => {resolve()});
        }
      });
    });
  }

  /** 
   * Whether the location is ready (to be read).
   * @returns promise resolving when ready
   */
  public isReady(): Promise<void> {
    return this._ready;
  }

  /** 
   * Scan directory recursively for locations. Sort them.
   * @returns promise resolving once all children are ready
   */
  private scan(): Promise<void> {
    let children: ZDocLocation[] = [];
    let childrenReady: Promise<void>[] = [];
    return new Promise((resolve, reject) => {
      fs.readdir(this.path, (err, files) => {
        // acquire all child items
        for (const file of files) {
          const name = this.path + '/' + file;
          const loc = new ZDocLocation(name);
          children.push(loc);
          childrenReady.push(loc.isReady());
        }
        //... wait for them to be ready
        Promise.all(childrenReady).then(() => {
          //... sort them (files before folders, alphabetically)
          children.sort((a,b) => {
            if (a.stats.isFile() != b.stats.isFile()) {
              return +b.stats.isFile() - +a.stats.isFile();
            }
            return a.path.toLowerCase() > b.path.toLowerCase() ? +1 : -1;
          });
          this.children = children;
          //... then resolve
          resolve();
        });
      });
    });
  }

  /** Doc this location and children. Invokes appropriate doccer */
  public doc() {
    // check for ignores
    const path = this.path.replace(/^\.\//, ''); // replace leading ./
    for (const ignore of ZDoccer.ignores) {
      if (minimatch(path, ignore, {matchBase: true})) {
        return;
      }
    }
    if (this.stats.isFile()) {
      // extract extension and doc accordingly
      const matches = this.path.match(/\.([\w\d]+)$/);
      const extension = matches ? matches[1] : '';
      if (ZDoccer.javadocExtensions.includes(extension)) {
        // console.log('javadoc', this.path);
        new ZDocJavadocSource(this.path);
      }
      else if (ZDoccer.markdownExtensions.includes(extension)) {
        // console.log('markdown', this.path);
        new ZDocMarkdownSource(this.path);
      }
    }
    else if (this.stats.isDirectory()) {
      for (const c of this.children || []) {
        c.doc();
      }
    }
  }
}

/**
 * Files to include in output, markdowned.
 * Every newly created `ZDocFile` is automatically stored in internal storage.
 */
class ZDocFile {
  /** all files */
  public static files: ZDocFile[] = [];
  /** this file's location */
  public path: string;
  /** this file's markdown content */
  public markdown: string;
  /** this file's linkable sections */
  public headings: ZDocHeading[] = [];

  /**
   * Create new file with content.
   * @param path path to the file (to display in header)
   * @param markdown markdown content of this file
   * @param header additional header info, content description
   */
  constructor(path: string, markdown: string, header: string) {
    this.path = path;
    // gather headings (+ aliases) and replace them with anchored headings
    markdown = markdown.replace(/^(\#+)(\(([\w-]+)\))?[ \t]+(.*)$/gm, (m, s1, s2, s3, s4) => {
      // console.log(m, 's1:', s1, 's2:', s2, 's3:', s3, 's4:', s4);
      const heading = new ZDocHeading(s4, s1.length, s3);
      this.headings.push(heading);
      return '<div id="' + heading.id +'"></div><!-- alias: ' + heading.alias + ' -->\n\n' + s1 + ' ' + heading.text;
    });
    // append file header
    this.markdown = '\n\n---\n\n*' + header + ' from ' + path +'*\n\n' + markdown;
    ZDocFile.files.push(this);
  }

  /** Resolve \[\[doc-style-links\]\](optional-link-target) in markdown */
  public resolveLinks() {
    // find '[[' anything, non-greedy ']]' optionally followed by '(' link ')'
    this.markdown = this.markdown.replace(/(\[\[.*?\]\])(\(.+?\))?/g, (m, g1, g2) => {
      // link text = group 1
      let linkText = g1;
      // trim whitespace from link text
      linkText = linkText.trim();
      // remove the square brackets
      linkText = linkText.replace(/^\[\[/, '');
      linkText = linkText.replace(/\]\]$/, '');
      // link target = group 2 - fallback to linkText
      let linkTarget = g2 || linkText;
      const considerAliases = g2 ? false : true;
      // turn link text into id-ish format
      const idPartial = ZDocHeading.createId(linkTarget);
      const idPartialRegExp = new RegExp('\\b' + idPartial + '\\b');
      // find decl with matching id
      let matchScore: number = 0;
      let matchHeading: ZDocHeading | undefined = undefined;
      let matchAmbiguities: ZDocHeading[] | null = null;
      let matchEvaluator = (headings: ZDocHeading[], partialScore: number) => {
        for (const heading of headings) {
          // evaluate current decl
          let score: number = 0;
          // exact match?
          if (
            idPartial == heading.id || 
            idPartial == heading.alias && considerAliases
          ) {
            score = 100;
          }
          else if (heading.id.match(idPartialRegExp)) {
            score = partialScore;
          }
          // better match than previous?
          if (score > matchScore) {
            matchScore = score;
            matchHeading = heading;
          }
          // ambiguous match?
          else if (score == 100 && score == matchScore) {
            if (!matchAmbiguities) {
              matchAmbiguities = [matchHeading as ZDocHeading];
            }
            matchAmbiguities.push(heading);
          }
          // exact match?
          // if (score == 100) return;
        }
      }
      //... first, in file
      matchEvaluator(this.headings, 2);
      //... then, if max score ain't reached yet, in files
      if (matchScore < 100) {
        for (const f of ZDocFile.files) {
          matchEvaluator(f.headings, 1);
          // if (matchScore == 1000) break;
        }
      }
      let icon: string = '**&#x1f875;**';
      //... emit warning for unresolved links
      if (!matchHeading) {
        console.warn('could not resolve link:', m);
        icon = '**&#x29b8;**';
      } else {
        //... for weakly resolved links
        if (matchScore < 100) {
          console.warn('weakly resolved link:', m);
          icon = '**?**';
        }
        //... for ambiguous links
        else if (matchAmbiguities) {
          console.warn('ambiguous link:', m);
          icon = '**&#x26a0;**';
        }
      }

      let id: string = matchHeading ? (matchHeading as ZDocHeading).id : '';
      return '[' + linkText + ' ' + icon + '](#' + id + ')';
    });
  }

  /** Emit this file's contents */
  public getContent(): string {
    return this.markdown;
  }
}

/**
 * Headings in files.
 */
class ZDocHeading {
  /** storage of all existing heading identifiers to prevent collisions */
  public static ids: string[] = [];
  /** text to display */
  public text: string;
  /** level */
  public level: number;
  /** id (unique) */
  public id: string;
  /** alias to be found by */
  public alias: string;

  /** Create and register new `ZDocHeading` from markdown heading */
  constructor(heading: string, level: number, alias: string) {
    this.level = level;
    // remove remaining leading and trailing whitespace
    this.text = heading.trim();
    // create id and assert it's free
    let idInitial = ZDocHeading.createId(this.text);
    let idTemp = idInitial;
    let idNr = 1;
    while (ZDocHeading.ids.indexOf(idTemp) > -1) {
      idNr++;
      idTemp = idInitial + '--' + idNr;
    }
    //... once found, set and register
    this.id = idTemp;
    // copy alias or id
    this.alias = alias ? alias : this.id;
    ZDocHeading.ids.push(this.id);
  }

  /** Create `id`-value from text */
  public static createId(text: string): string {
    // lowercase everything (might break some ambiguities, but anchors are CI)
    let id: string = text.toLowerCase();
    // turn all groups of non-word-characters into -
    id = id.replace(/[^a-z0-9]+/g, '-');
    // strip leading and trailing -
    id = id.replace(/^-/, '');
    id = id.replace(/-$/, '');
    return id;
  }
}

/**
 * Javadoc source transformer
 */
class ZDocJavadocSource {
  /**
   * Create new transformer.
   * Parse file for Javadoc documented declarations and transform them to
   * markdown.
   * If result is not empty, it's stored as [[ZDocFile]]
   */
  constructor(path: string) {
    let content: string = '';
    const source: string = fs.readFileSync(path, 'utf8');
    // extract Javadoc + declaration sections
    // spaces or tabs '/**' [anything, non-greedy] '*/',
    // line-break,
    // one whole line
    const regex = /(^[ \t]*\/\*\*(?!\/)#*[\S\s]*?\*\/)(?:\r\n|\r|\n)([^\r\n]*)/gm;
    let match: RegExpMatchArray | null;
    // by doing it this way instead of string.match, the groups are accessible
    do {
      match = regex.exec(source);
      if (match) {
        content += this.docMatch(match);
      }
    } while (match != null);
    // file generated content? store
    if (content != '') {
      new ZDocFile(path, content, 'transformed Javadoc');
    }
  }

  /**
   * Turn a Javadoc + declaration match into markdown
   */
  private docMatch(match: RegExpMatchArray): string {
    let doc: string = match[1];
    let indentOverwrite: number|undefined = undefined;
    // strip the /** */ from javadoc lines
    doc = doc.replace(/^[ \t]*\/\*\*[ \t]*/, '');
    doc = doc.replace(/[ \t]*\*\/[ \t]*$/, '');
    // strip the * from intermediate javadoc lines
    doc = doc.replace(/^[ \t]*\* ?/gm, '');
    // strip indentation overwrite
    doc = doc.replace(/^(#+)[ \t]*/, (m, g1) => {
      indentOverwrite = g1.length;
      return '';
    });
    // prettify Javadoc params
    // (params first, they're a bit special)
    doc = doc.replace(/^\@param\s+(\w+)\s+(.*)/gm, (m, g1, g2) => {
      return '- *param* `' + g1 + '` &mdash; ' + g2;
    });
    //... then bindings (they're not standard)
    doc = doc.replace(/^\@binding\s+(\S+)\s+(.*)/gm, (m, g1, g2) => {
      return '- *binding* `' + g1 + '` &mdash; ' + g2;
    });
    //... then slots (they're not standard)
    doc = doc.replace(/^\@slot\s+(\S+)\s+(.*)/gm, (m, g1, g2) => {
      return '- *slot* `' + g1 + '` &mdash; ' + g2;
    });
    //... then the rest
    doc = doc.replace(/^\@(\w+)\s+(.*)/gm, (m, g1, g2) => {
      return '- *'+ g1 + '* &mdash; ' + g2;
    });
    let decl: string = match[2];
    let md = '';
    // non-empty line? append as header
    if (decl.trim() != '') {
      // indentation
      let indent: number = 0;
      // auto-determine indentation by counting space-pairs or tabs
      if (!indentOverwrite) {
        // add two per default
        indent = 2;
        decl = decl.replace(/^(  |\t)/g, (t) => {
          indent++;
          return '';
        });
      }
      // overwritten indentation
      else {
        indent = indentOverwrite;
      }
      // strip decor from decl line
      decl = decl.replace(/^\s*/, '');
      decl = decl.replace(/\s*[{;]?$/, '');
      // strip //doc from decl line (this is for doccing pseudo decls)
      decl = decl.replace(/^\/\/doc\s*/, '');
      // mangle, then trim again
      for (const mangle of ZDoccer.manglesRegex) {
        decl = decl.replace(mangle, '');
        decl = decl.trim();
      }
      // create alias, by stripping keywords and typings and params
      let alias = decl;
      alias = alias.replace(/:.*/, '');
      alias = alias.replace(/\(.*/, '');
      alias = alias.replace(/\?.*/, '');
      alias = alias.replace(/<.*/, '');
      alias = alias.replace(/=.*/, '');
      alias = alias.replace(/^export /, '');
      alias = alias.replace(/^public /, '');
      alias = alias.replace(/^private /, '');
      alias = alias.replace(/^static /, '');
      alias = alias.replace(/^async /, '');
      alias = alias.replace(/^readonly /, '');
      alias = alias.replace(/^abstract /, '');
      alias = alias.replace(/^class /, '');
      alias = alias.replace(/^type /, '');
      alias = alias.replace(/^function /, '');
      alias = alias.replace(/^interface /, '');
      alias = alias.replace(/^const /, '');
      alias = alias.replace(/^enum /, '');
      alias = alias.replace(/ implements .*/, '');
      alias = alias.replace(/ extends .*/, '');
      alias = ZDocHeading.createId(alias);
      // turn to markdown
      md += '#'.repeat(indent) + '(' + alias + ') `' + decl + '`\n\n';
    }
    md += doc;
    md += '\n\n';
    return md;
  }
}

/**
 * Markdown source transformer
 */
class ZDocMarkdownSource {
  /**
   * Create new transformer.
   * Parse file for markdown.
   * If result is not empty, it's stored as [[ZDocFile]]
   */
  constructor(path: string) {
    const content: string = fs.readFileSync(path, 'utf8');
    // file generated content? store
    if (content != '') {
      new ZDocFile(path, content, 'original Markdown');
    }
  }
}

new ZDoccer();
