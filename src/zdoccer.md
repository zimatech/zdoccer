# ZDoccer

This plain typescript project was initialized with
```
tsc --init
npm init
npm i @types/node --save-dev
```

## Build

To build, run
```
npm run build
```

## Publish

To publish ZDoccer, run
```
npm publish --access=public
```

## Update `TEST.md`

To do a test doc run, run
```
npm run test
```
