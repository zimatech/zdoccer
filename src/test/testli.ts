
/**
 * conflicting class [[conflict]]
 */
class Conflict {
  /** this function is marked as async but should be resolvable [[thisOnesAsync]] */
  public async thisOnesAsync() {

  }
  
  public thisOnesEmpty() {
    /**/
  }

  public thisOnesEmptyToo() {
    /**/
  }
}