Should be ambiguous: [[Conflict]]

Should not be ambiguous: [[Conflict]](class-conflict)

Should not be ambiguous: [[Conflict]](conflict)

Should not be resolved: [[class-no-target-here]]

Should not be resolved: [[Conflict]](tortellini-conflict)

Should be marked as weak: [[php]]